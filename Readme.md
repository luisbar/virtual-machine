![Semantic description of image](/screenshot/first.png)

#### How to run the project

```sh
mkdir -p output && javac ./src/com/compiladores/*/*.java -d output && java -cp output com.compiladores.presentation.MainFrm
```

#### How to use

- Load the file called test (root folder of the project) pressing F1
- Press F3 for processing data step by step
- Press F4 for jumping to the next line

:warning: If you want to process al data in a quick way, you have to press F2 after loading the test file