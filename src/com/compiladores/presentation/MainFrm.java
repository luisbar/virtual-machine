package com.compiladores.presentation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import com.compiladores.bussines.Codigo3;
import com.compiladores.bussines.Interprete;

public class MainFrm extends JFrame implements ActionListener,KeyListener {

	private static final long serialVersionUID = 2L;
	private JMenuItem itemFileOpen;
	private JMenuItem itemRunAll;
	private JMenuItem itemDebug;
	private JTable tsvar;
	private JTable tss;
	private JTextArea file;
	private Codigo3 c3;
	private JTable stack;
	private JTextArea console;
	private JMenuItem itemNext;

	public MainFrm() {
		initComponents();
		c3 = new Codigo3();
	}

	private void initComponents() {
		// configuracion de mainfrm
		Toolkit tool = this.getToolkit();
		Dimension dim = tool.getScreenSize();
		this.setSize(dim);
		this.setTitle("Virtual Machine");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new BorderLayout());

		// creacion de componentes
		createNorth();
		createSouth();
		createEast();
		createCenter();

		this.setVisible(true);
	}

	private void createEast() {
		String[] titleStack = { "Pila", "Ambito" };
		DefaultTableModel modelStack = new DefaultTableModel(null, titleStack);

		stack = new JTable(modelStack);
		JScrollPane scrollStack = new JScrollPane(stack);

		String[] titleTsvar = { "Nombre", "ValorI", "ValorF", "Cant. Tmp." };
		DefaultTableModel modelTsvar = new DefaultTableModel(null, titleTsvar);

		tsvar = new JTable(modelTsvar);
		JScrollPane scrollTsvar = new JScrollPane(tsvar);

		String[] titleTss = { "TSS" };
		DefaultTableModel modelTss = new DefaultTableModel(null, titleTss);

		tss = new JTable(modelTss);
		JScrollPane scrollTss = new JScrollPane(tss);

		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());

		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		gbc.fill = GridBagConstraints.BOTH;
		panel.add(scrollTsvar, gbc);

		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		panel.add(scrollTss, gbc);

		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		panel.add(scrollStack, gbc);

		this.add(panel, BorderLayout.EAST);
	}

	private void createCenter() {
		file = new JTextArea();
		file.setBackground(Color.DARK_GRAY);
		file.setForeground(Color.WHITE);
		file.addKeyListener(this);

		JScrollPane scroll = new JScrollPane(file);
		scroll.setBorder(null);

		this.add(scroll, BorderLayout.CENTER);
	}

	private void createSouth() {
		console = new JTextArea();
		console.setRows(10);
		console.setBackground(Color.WHITE);
		console.setBorder(new TitledBorder("Consola"));

		JScrollPane scroll = new JScrollPane(console);

		this.add(scroll, BorderLayout.SOUTH);
	}

	private void createNorth() {
		// creacion de componentes
		JMenuBar menuBar = new JMenuBar();
		JMenu menuFile = new JMenu("File");
		JMenu menuRun = new JMenu("Project");
		itemFileOpen = new JMenuItem("Open (F1)");
		itemRunAll = new JMenuItem("Run (F2)");
		itemDebug = new JMenuItem("Debug (F3)");
		itemNext = new JMenuItem("Next (F4)");

		// configuracion de componentes
		menuBar.setBackground(Color.DARK_GRAY);
		menuBar.setBorder(null);

		menuFile.setForeground(Color.WHITE);
		menuFile.setBorder(null);

		menuRun.setForeground(Color.WHITE);
		menuRun.setBorder(null);

		itemFileOpen.setForeground(Color.WHITE);
		itemFileOpen.setBackground(Color.DARK_GRAY);
		itemFileOpen.addActionListener(this);

		itemRunAll.setForeground(Color.WHITE);
		itemRunAll.setBackground(Color.DARK_GRAY);
		itemRunAll.addActionListener(this);

		itemDebug.setForeground(Color.WHITE);
		itemDebug.setBackground(Color.DARK_GRAY);
		itemDebug.addActionListener(this);
		
		itemNext.setForeground(Color.WHITE);
		itemNext.setBackground(Color.DARK_GRAY);
		itemNext.addActionListener(this);
		
		// agregacion de componentes
		menuFile.add(itemFileOpen);
		menuRun.add(itemRunAll);
		menuRun.add(itemDebug);
		menuRun.add(itemNext);

		menuBar.add(menuFile);
		menuBar.add(menuRun);

		this.getContentPane().add(menuBar, BorderLayout.NORTH);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == itemFileOpen) {
			// creacion y configuracion filechooser
			JFileChooser fileChooser = new JFileChooser();
			int value = fileChooser.showOpenDialog(fileChooser);

			if (value == JFileChooser.APPROVE_OPTION) {
				File fileIn = fileChooser.getSelectedFile();
				c3.Open(fileIn.getName());
				c3.getTSVar().loadTableTsvar(tsvar);
				c3.getTSS().loadTableTss(tss);
				c3.loadC3(file);
			}
		}

		if (arg0.getSource() == itemRunAll) {
			Interprete interprete = new Interprete(this.c3, tsvar, tss, stack, console, file);
			interprete.execute();
		}

		if (arg0.getSource() == itemDebug) {
			Interprete interprete = new Interprete(this.c3, tsvar, tss, stack, console, file);
			Interprete.debug = true;
			interprete.execute();
		}
		
		if(arg0.getSource() == itemNext){
			Interprete.next = true;
		}
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new MainFrm();
			}
		});
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		if(arg0.getKeyCode() == KeyEvent.VK_F1){
			JFileChooser fileChooser = new JFileChooser();
			int value = fileChooser.showOpenDialog(fileChooser);

			if (value == JFileChooser.APPROVE_OPTION) {
				File fileIn = fileChooser.getSelectedFile();
				c3.Open(fileIn.getName());
				c3.getTSVar().loadTableTsvar(tsvar);
				c3.getTSS().loadTableTss(tss);
				c3.loadC3(file);
			}
		}
		
		if(arg0.getKeyCode() == KeyEvent.VK_F2){
			Interprete interprete = new Interprete(this.c3, tsvar, tss, stack, console, file);
			interprete.execute();
		}
		
		if(arg0.getKeyCode() == KeyEvent.VK_F3){
			Interprete interprete = new Interprete(this.c3, tsvar, tss, stack, console, file);
			Interprete.debug = true;
			interprete.execute();
		}
		
		if(arg0.getKeyCode() == KeyEvent.VK_F4){
			Interprete.next = true;
		}
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {	
	}

	@Override
	public void keyTyped(KeyEvent arg0) {	
	}
}
