package com.compiladores.presentation;

import java.awt.Color;
import java.awt.Component;
import java.util.Random;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class TableFormat extends DefaultTableCellRenderer {

	private static final long serialVersionUID = 1L;
	private Color color;
	private int rowCount;
	
	
	public TableFormat(int rowCount){
		color = Color.decode(new Random().nextInt(999999) + "");
		this.rowCount = rowCount;
		System.out.println("cons " + this.rowCount);
	}
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		super.getTableCellRendererComponent(table, value, isSelected, hasFocus,
				row, column);
		System.out.println("met" + this.rowCount);
		if(row > this.rowCount){
			setBackground(color);
			rowCount = row;
		}

		return this;
	}

}
