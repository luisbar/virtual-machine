package com.compiladores.bussines;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.Caret;

public class Interprete extends SwingWorker<Void, String>{
	private Stack<Object> stack;
	private HashMap<Integer,Integer> listE;
	private DefaultTableModel model;
	private LinkedList<String> chunks;
	
	private static final int NAME = 1;
	private static final int VALUE_I = 2;
	private static final int VALUE_F = 3;
	private static final int NUMBER_TMP = 4;
	
	private boolean call;
	public static boolean debug;
	public static boolean next;
	
	private int var1;
	private int var2;
	private int IP;
	private int returnDir;
	
	private String[] lst;
	
	private Codigo3 c3;
	private JTable tsvar;
	private JTable tss;
	private JTable tableStack;
	private JTextArea console;
	private JTextArea file;
	
	public Interprete(Codigo3 c3, JTable tsvar, JTable tss, JTable tableStack, JTextArea console, JTextArea file){
		this.stack = new Stack<Object>();
		this.listE = new HashMap<Integer,Integer>();
		this.c3 = c3;
		this.tsvar =  tsvar;
		this.tss = tss;
		this.tableStack = tableStack;
		this.console = console;
		this.lst = new String[1];
		this.chunks = new LinkedList<String>();
		this.model = (DefaultTableModel) tableStack.getModel();
		this.model.setRowCount(0);
		this.debug = false;
		this.next = true;
		this.file = file;
	}
	@Override
	protected Void doInBackground() throws Exception {
		saveE(c3);
		
		IP = (int) getValueOfTsvar(c3, VALUE_I, existProc(c3, "$main"));
		publish("saveInStack","-1", "true");
		Thread.sleep(100);
		
		for (int i = 0; i < getNumberTmp(c3, existProc(c3, "$main")); i++) {//agrego cantidad de temporales
			publish("saveInStack","0", "false");
			Thread.sleep(100);
		}
		
		while(IP != -1){//mientras no sea fin de proceso
			selectRow(IP, file, c3);
			
			if(debug)
				next = false;//para el debug
			
			while(!next){Thread.sleep(100);};
			
			switch (c3.getCuadrupla(IP).getTipoOp()){
				case Cuadrupla.OPCALL:
					publish("saveInStack", IP+1 + "", "true");
					Thread.sleep(100);
					
					for (int i = 0; i < getNumberTmp(c3, getDirOfCuadrupla(1, c3)*-1); i++) {//agrego cantidad de temporales
						publish("saveInStack", "0", "false");
						Thread.sleep(100);
					}
					
					IP = (int) getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(1, c3)*-1);//cambio mi IP al nuevo contexto
					call = false;
					break;
					
				case Cuadrupla.ETIQUETA:

					break;
					
				case Cuadrupla.OPWRITES:
					publish("write", getValueOfTss(c3, getDirOfCuadrupla(1, c3) * -1));
					break;
					
				case Cuadrupla.OPREAD:
					//falta para temporales
					String val = JOptionPane.showInputDialog(c3.getCuadrupla(IP).toString(c3));

					c3.getTSVar().setValorI(existProc(c3, (String) getValueOfTsvar(c3, NAME, getDirOfCuadrupla(1, c3)*-1)), Integer.parseInt(val));//cambio valor de la variable en la tsvar
					
					tsvar.setValueAt(val, existProc(c3, (String) getValueOfTsvar(c3, NAME, getDirOfCuadrupla(1, c3)*-1)), 1);//cambio valor de la variable en la tabla
					break;
					
				case Cuadrupla.OPASIGNNUM:
					if(c3.getCuadrupla(IP).getDir(1) > 0){//es temporal
						this.stack.set(returnDir+getDirOfCuadrupla(1, c3), getDirOfCuadrupla(2, c3));
						publish("setValueAt", getDirOfCuadrupla(2, c3) + "", returnDir + getDirOfCuadrupla(1, c3) + "", 0 + "");
					}else{//es global
						c3.getTSVar().setValorI(getDirOfCuadrupla(1, c3)*-1, getDirOfCuadrupla(2, c3));
						tsvar.setValueAt(getDirOfCuadrupla(2, c3), getDirOfCuadrupla(1, c3)*-1, 1);
					}
					Thread.sleep(100);
					break;
					
				case Cuadrupla.OPIF0:
					int aux = (int)(this.stack.get(getDirOfCuadrupla(1, c3) + returnDir));
					if( aux == 0){
						IP = listE.get(getDirOfCuadrupla(2, c3));
					}
					break;
					
				case Cuadrupla.OPIF1:
					if((int)(this.stack.get(getDirOfCuadrupla(1, c3) + returnDir)) == 1){
						IP = listE.get(getDirOfCuadrupla(2, c3));
					}
					break;
					
				case Cuadrupla.OPGOTO:
					IP = listE.get(getDirOfCuadrupla(1, c3));
					break;
					
				case Cuadrupla.OPMAI:
					var1 = (int) (getDirOfCuadrupla(2, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(2, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(2, c3)));
					var2 = (int) (getDirOfCuadrupla(3, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(3, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(3, c3)));
					
					if(var1 >= var2){
						this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 1);
						publish("setValueAt", 1 + "", returnDir + getDirOfCuadrupla(1, c3) + "", 0 + "");
					}else{
						this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 0);
						publish("setValueAt", 0 + "", returnDir + getDirOfCuadrupla(1, c3) + "", 0 + "");
					}
					Thread.sleep(100);
					break;
					
				case Cuadrupla.OPMEI:
					var1 = (int) (getDirOfCuadrupla(2, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(2, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(2, c3)));
					var2 = (int) (getDirOfCuadrupla(3, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(3, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(3, c3)));
					
					if(var1 <= var2){
						this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 1);
						publish("setValueAt", 1 + "", returnDir + getDirOfCuadrupla(1, c3) + "", 0 + "");
					}else{
						this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 0);
						publish("setValueAt", 0 + "", returnDir + getDirOfCuadrupla(1, c3) + "", 0 + "");
					}
					Thread.sleep(100);
					break;
					
				case Cuadrupla.OPMAY:
					var1 = (int) (getDirOfCuadrupla(2, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(2, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(2, c3)));
					var2 = (int) (getDirOfCuadrupla(3, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(3, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(3, c3)));
					
					if(var1 > var2){
						this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 1);
						publish("setValueAt", 1 + "", returnDir + getDirOfCuadrupla(1, c3) + "", 0 + "");
					}else{
						this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 0);
						publish("setValueAt", 0 + "", returnDir + getDirOfCuadrupla(1, c3) + "", 0 + "");
					}
					Thread.sleep(100);
					break;
					
				case Cuadrupla.OPMEN:
					var1 = (int) (getDirOfCuadrupla(2, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(2, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(2, c3)));
					var2 = (int) (getDirOfCuadrupla(3, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(3, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(3, c3)));
					
					if(var1 < var2){
						this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 1);
						publish("setValueAt", 1 + "", returnDir + getDirOfCuadrupla(1, c3) + "", 0 + "");
					}else{
						this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 0);
						publish("setValueAt", 0 + "", returnDir + getDirOfCuadrupla(1, c3) + "", 0 + "");
					}
					Thread.sleep(100);
					break;
					
				case Cuadrupla.OPIGUAL:
					var1 = (int) (getDirOfCuadrupla(2, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(2, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(2, c3)));
					var2 = (int) (getDirOfCuadrupla(3, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(3, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(3, c3)));
					
					if(var1 == var2){
						this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 1);
						publish("setValueAt", 1 + "", returnDir + getDirOfCuadrupla(1, c3) + "", 0 + "");
					}else{
						this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 0);
						publish("setValueAt", 0 + "", returnDir + getDirOfCuadrupla(1, c3) + "", 0 + "");
					}
					Thread.sleep(100);
					break;
					
				case Cuadrupla.OPDIS:
					var1 = (int) (getDirOfCuadrupla(2, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(2, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(2, c3)));
					var2 = (int) (getDirOfCuadrupla(3, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(3, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(3, c3)));
					
					if(var1 != var2){
						this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 1);
						publish("setValueAt", 1 + "", returnDir + getDirOfCuadrupla(1, c3) + "", 0 + "");
					}else{
						this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 0);
						publish("setValueAt", 0 + "", returnDir + getDirOfCuadrupla(1, c3) + "", 0 + "");
					}
					Thread.sleep(100);
					break;
					
				case Cuadrupla.OPRET:
					var1 = (this.stack.size()-1)- returnDir;

					for (int i = 0; i < var1; i++) {
						this.stack.pop();
					}
					
					var1++;
					model.setRowCount(model.getRowCount() - var1 );
					
					IP = (int) this.stack.pop();
					call = false;
					returnDir = this.stack.size()-1;
					break;
					
				case Cuadrupla.OPINC:
					//falta para temporales
					var1 = (int) getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(1, c3)*-1);
					var1++;
					setValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(1, c3)*-1, var1);
					tsvar.setValueAt(var1, existProc(c3, (String) getValueOfTsvar(c3, NAME, getDirOfCuadrupla(1, c3)*-1)), 1);
					break;
			}
			Thread.sleep(100);
			
			if(call)
				IP++;
			else
				call = true;
		}	
		publish("write","FIN DEL PROCESO!!!");
		return null;
	}
	
	/**
	 * @param iP2
	 * @param file
	 * @param c3
	 * @return selecciona la fila correspondiente a la IP
	 */
	private void selectRow(int iP2, JTextArea file, Codigo3 c3) {
		String allText = file.getText();
		int initPos = allText.indexOf(IP + ".");
		int endPos = initPos + String.valueOf(IP+".").length() + 2 + c3.getCuadrupla(IP).toString(c3).length();
		file.select(initPos, endPos);
	}
	
	@Override
	protected void process(List<String> chunks) {
		if(chunks.get(0).equals("saveInStack")){
			saveInStack(lst, Integer.valueOf(chunks.get(1)), model, tableStack, Boolean.valueOf(chunks.get(2)));
		}
		
		if(chunks.get(0).equals("saveValueAt")){
			tableStack.setValueAt(chunks.get(1), Integer.valueOf(chunks.get(2)), Integer.valueOf(chunks.get(3)));
		}
		
		if(chunks.get(0).equals("write")){
			console.append(chunks.get(1) + "\n");
		}
	}
	/**
	 * @param c3
	 * @param nameTsvar
	 * @return devuelve la posicion de donde esta almacenado en la tsvar un procedimiento, 
	 * el cual se lo obtiene introduciendo el nombre de dicho procedimiento
	 */
	private int existProc(Codigo3 c3, String nameTsvar){
		return c3.getTSVar().Existe(nameTsvar);
	}
	/**
	 * @param lst
	 * @param num
	 * @param model
	 * @param stack
	 * @param returnDir
	 * almacena en la pila los temporales y las direcciones de retorno
	 */
	private void saveInStack(String[] lst, int num, DefaultTableModel model, JTable stack, boolean returnDir){
		lst[0] = num + "";
		model.addRow(lst);
		stack.setModel(model);
		this.stack.push(num);
		
		if(returnDir)
			this.returnDir = this.stack.size()-1;
		
	}
	/**
	 * @param index
	 * @param c3
	 * @return retorna valor de un campo de una cuadrupla
	 */
	private int getDirOfCuadrupla(int index, Codigo3 c3){
		return c3.getCuadrupla(IP).getDir(index);
	}
	/**
	 * @param c3
	 * @param action
	 * @param posTsvar
	 * @return retorna cualquier valor de la tsvar, introduciendo el index de la tupla a la que se desea acceder
	 */
	private Object getValueOfTsvar(Codigo3 c3, int action, int posTsvar){
		switch(action){
		case NAME:
			return c3.getTSVar().getNombreID(posTsvar);

		case VALUE_I:
			return c3.getTSVar().getValorI(posTsvar);
			
		case VALUE_F:
			return c3.getTSVar().getValorF(posTsvar);
			
		case NUMBER_TMP:
			return c3.getTSVar().getCantTmp(posTsvar);
		}
		
		return null;
	}
	/**
	 * @param c3
	 * @param posTs
	 * @return retorna el numero de temporales que tiene un proceso
	 */
	private int getNumberTmp(Codigo3 c3, int posTs){
		return c3.getTSVar().getCantTmp(posTs);
	}
	/**
	 * @param c3
	 * @param action
	 * @return retorna el cualquier valor de la tsvar, introduciendo la IP, si no es una variable global
	 * o un procedimiento el que se esta leyendo devolvera null
	 */
//	private Object getValueOfTsvar(Codigo3 c3, int action){//no sirve!!!
//		switch(action){
//		case NAME:
//			return c3.getTSVar().getTupla(IP).getNombreID();
//
//		case VALUE_I:
//			return c3.getTSVar().getTupla(IP).getValorI();
//			
//		case VALUE_F:
//			return c3.getTSVar().getTupla(IP).getValorF();
//			
//		case NUMBER_TMP:
//			return c3.getTSVar().getTupla(IP).getCantTmp();
//		}
//		
//		return null;
//	}
	/**
	 * @param c3
	 * @param index
	 * @return retorna el valor de un campo de la tss
	 */
	private String getValueOfTss(Codigo3 c3, int index){
		return c3.getTSS().getStr(index);
	}
	/**
	 * @param c3
	 * @param action
	 * @param index
	 * @param value
	 * @return almacena valor en el campo respectivo de la tsvar
	 */
	private void setValueOfTsvar(Codigo3 c3, int action, int index, Object value){
		switch(action){
		case VALUE_I:
			c3.getTSVar().setValorI(index, (int) value);
			
		case VALUE_F:
			c3.getTSVar().setValorF(index, (int) value);
		}
	}
	/**
	 * @param c3
	 * @return guarda las direcciones de todas las etiquetas en un hash map
	 */
	private void saveE(Codigo3 c3){
		while (IP < c3.getN()) {
			if(Cuadrupla.ETIQUETA == c3.getCuadrupla(IP).getTipoOp()){
				listE.put(c3.getCuadrupla(IP).getDir(1), IP);
			}
			IP += 1;
		}

		IP = 0;
	}
}
