package com.compiladores.bussines;

import java.util.HashMap;
import java.util.Stack;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

public class Codigo_3 {
	private int IP;
	private int returnDir;
	private Stack<Object> stack;
	private boolean call;
	private HashMap<Integer,Integer> listE;
	
	private static final int NAME = 1;
	private static final int VALUE_I = 2;
	private static final int VALUE_F = 3;
	private static final int NUMBER_TMP = 4;
	
	private boolean debug;

	public Codigo_3() {
		stack = new Stack<Object>();
		listE = new HashMap<Integer,Integer>();
	}

	public void interpretar(Codigo3 c3, JTable tsvar, JTable tss, JTable stack, JTextArea console) {
		saveE(c3);
		IP = (int) getValueOfTsvar(c3, VALUE_I, existProc(c3, "$main"));

		String[] lst = new String[1];
		DefaultTableModel model;

		if (IP != -1) {
			model = (DefaultTableModel) stack.getModel();
			model.setRowCount(0);
			saveInStack(lst, -1, model, stack, true);//para fin de proceso

			for (int i = 0; i < getNumberTmp(c3, existProc(c3, "$main")); i++) {//agrego cantidad de temporales
				saveInStack(lst, 0, model, stack, false);
			}

			while(IP != -1){//mientras no sea fin de proceso
				switch (c3.getCuadrupla(IP).getTipoOp()){
					case Cuadrupla.OPCALL:
						saveInStack(lst, IP+1, model, stack, true);//guardo direccion de retorno

						for (int i = 0; i < getNumberTmp(c3, getDirOfCuadrupla(1, c3)*-1); i++) {//agrego cantidad de temporales
							saveInStack(lst, 0, model, stack, false);
						}
						
						IP = (int) getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(1, c3)*-1);//cambio mi IP al nuevo contexto
						call = false;
						break;
						
					case Cuadrupla.ETIQUETA:
						//no hace nada
						break;
						
					case Cuadrupla.OPWRITES:
						console.append(getValueOfTss(c3, getDirOfCuadrupla(1, c3) * -1) + "\n");
						break;
						
					case Cuadrupla.OPREAD:
						//falta para temporales
						String val = JOptionPane.showInputDialog(c3.getCuadrupla(IP).toString(c3));

						c3.getTSVar().setValorI(existProc(c3, (String) getValueOfTsvar(c3, NAME, getDirOfCuadrupla(1, c3)*-1)), Integer.parseInt(val));//cambio valor de la variable en la tsvar
						
						tsvar.setValueAt(val, existProc(c3, (String) getValueOfTsvar(c3, NAME, getDirOfCuadrupla(1, c3)*-1)), 1);//cambio valor de la variable en la tabla
						break;
						
					case Cuadrupla.OPASIGNNUM:
						if(c3.getCuadrupla(IP).getDir(1) > 0){//es temporal
							this.stack.set(returnDir+getDirOfCuadrupla(1, c3), getDirOfCuadrupla(2, c3));
							stack.setValueAt(getDirOfCuadrupla(2, c3), returnDir+getDirOfCuadrupla(1, c3), 0);
						}else{//es global
							c3.getTSVar().setValorI(getDirOfCuadrupla(1, c3)*-1, getDirOfCuadrupla(2, c3));
							tsvar.setValueAt(getDirOfCuadrupla(2, c3), getDirOfCuadrupla(1, c3)*-1, 1);
						}
						break;
						
					case Cuadrupla.OPIF0:
						//esta mal
						int aux = (int)(this.stack.get(getDirOfCuadrupla(1, c3) + returnDir));
						if( aux == 0){
							IP = listE.get(getDirOfCuadrupla(2, c3));
						}
						break;
						
					case Cuadrupla.OPIF1:
						//esta mal
						if((int)(this.stack.get(getDirOfCuadrupla(1, c3) + returnDir)) == 1){
							IP = listE.get(getDirOfCuadrupla(2, c3));
						}
						break;
						
					case Cuadrupla.OPGOTO:
						//esta mal
						IP = listE.get(getDirOfCuadrupla(1, c3));
						break;
						
					case Cuadrupla.OPMAI:
						int var1 = (int) (getDirOfCuadrupla(2, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(2, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(2, c3)));
						int var2 = (int) (getDirOfCuadrupla(3, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(3, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(3, c3)));
						
						if(var1 >= var2){
							this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 1);
							stack.setValueAt(1, returnDir + getDirOfCuadrupla(1, c3), 0);
						}else{
							this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 0);
							stack.setValueAt(0, returnDir + getDirOfCuadrupla(1, c3), 0);
						}
						break;
						
					case Cuadrupla.OPMEI:
						var1 = (int) (getDirOfCuadrupla(2, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(2, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(2, c3)));
						var2 = (int) (getDirOfCuadrupla(3, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(3, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(3, c3)));
						
						if(var1 <= var2){
							this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 1);
							stack.setValueAt(1, returnDir + getDirOfCuadrupla(1, c3), 0);
						}else{
							this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 0);
							stack.setValueAt(0, returnDir + getDirOfCuadrupla(1, c3), 0);
						}
						break;
						
					case Cuadrupla.OPMAY:
						var1 = (int) (getDirOfCuadrupla(2, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(2, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(2, c3)));
						var2 = (int) (getDirOfCuadrupla(3, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(3, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(3, c3)));
						
						if(var1 > var2){
							this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 1);
							stack.setValueAt(1, returnDir + getDirOfCuadrupla(1, c3), 0);
						}else{
							this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 0);
							stack.setValueAt(0, returnDir + getDirOfCuadrupla(1, c3), 0);
						}
						break;
						
					case Cuadrupla.OPMEN:
						var1 = (int) (getDirOfCuadrupla(2, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(2, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(2, c3)));
						var2 = (int) (getDirOfCuadrupla(3, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(3, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(3, c3)));
						
						if(var1 < var2){
							this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 1);
							stack.setValueAt(1, returnDir + getDirOfCuadrupla(1, c3), 0);
						}else{
							this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 0);
							stack.setValueAt(0, returnDir + getDirOfCuadrupla(1, c3), 0);
						}
						break;
						
					case Cuadrupla.OPIGUAL:
						var1 = (int) (getDirOfCuadrupla(2, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(2, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(2, c3)));
						var2 = (int) (getDirOfCuadrupla(3, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(3, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(3, c3)));
						
						if(var1 == var2){
							this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 1);
							stack.setValueAt(1, returnDir + getDirOfCuadrupla(1, c3), 0);
						}else{
							this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 0);
							stack.setValueAt(0, returnDir + getDirOfCuadrupla(1, c3), 0);
						}
						break;
						
					case Cuadrupla.OPDIS:
						var1 = (int) (getDirOfCuadrupla(2, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(2, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(2, c3)));
						var2 = (int) (getDirOfCuadrupla(3, c3)<0 ? getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(3, c3) * -1) : this.stack.get(returnDir + getDirOfCuadrupla(3, c3)));
						
						if(var1 != var2){
							this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 1);
							stack.setValueAt(1, returnDir + getDirOfCuadrupla(1, c3), 0);
						}else{
							this.stack.set(returnDir + getDirOfCuadrupla(1, c3), 0);
							stack.setValueAt(0, returnDir + getDirOfCuadrupla(1, c3), 0);
						}
						break;
						
					case Cuadrupla.OPRET:
						int aux1 = (this.stack.size()-1)- returnDir;

						for (int i = 0; i < aux1; i++) {
							this.stack.pop();
						}
						
						IP = (int) this.stack.pop();
						call = false;
						returnDir = this.stack.size()-1;
						break;
						
					case Cuadrupla.OPINC:
						//falta para temporales
						int var3 = (int) getValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(1, c3)*-1);
						var3++;
						setValueOfTsvar(c3, VALUE_I, getDirOfCuadrupla(1, c3)*-1, var3);
						break;
				}
				if(call)
					IP++;
				else
					call = true;
			}
			
		}

	}
	/**
	 * @param c3
	 * @param nameTsvar
	 * @return devuelve la posicion de donde esta almacenado en la tsvar un procedimiento, 
	 * el cual se lo obtiene introduciendo el nombre de dicho procedimiento
	 */
	private int existProc(Codigo3 c3, String nameTsvar){
		return c3.getTSVar().Existe(nameTsvar);
	}
	/**
	 * @param lst
	 * @param num
	 * @param model
	 * @param stack
	 * @param returnDir
	 * almacena en la pila los temporales y las direcciones de retorno
	 */
	private void saveInStack(String[] lst, int num, DefaultTableModel model, JTable stack, boolean returnDir){
		lst[0] = num + "";
		model.addRow(lst);
		stack.setModel(model);
		this.stack.push(num);
		
		if(returnDir)
			this.returnDir = this.stack.size()-1;
		
	}
	/**
	 * @param index
	 * @param c3
	 * @return retorna valor de un campo de una cuadrupla
	 */
	private int getDirOfCuadrupla(int index, Codigo3 c3){
		return c3.getCuadrupla(IP).getDir(index);
	}
	/**
	 * @param c3
	 * @param action
	 * @param posTsvar
	 * @return retorna cualquier valor de la tsvar, introduciendo el index de la tupla a la que se desea acceder
	 */
	private Object getValueOfTsvar(Codigo3 c3, int action, int posTsvar){
		switch(action){
		case NAME:
			return c3.getTSVar().getNombreID(posTsvar);

		case VALUE_I:
			return c3.getTSVar().getValorI(posTsvar);
			
		case VALUE_F:
			return c3.getTSVar().getValorF(posTsvar);
			
		case NUMBER_TMP:
			return c3.getTSVar().getCantTmp(posTsvar);
		}
		
		return null;
	}
	/**
	 * @param c3
	 * @param posTs
	 * @return retorna el numero de temporales que tiene un proceso
	 */
	private int getNumberTmp(Codigo3 c3, int posTs){
		return c3.getTSVar().getCantTmp(posTs);
	}
	/**
	 * @param c3
	 * @param action
	 * @return retorna el cualquier valor de la tsvar, introduciendo la IP, si no es una variable global
	 * o un procedimiento el que se esta leyendo devolvera null
	 */
//	private Object getValueOfTsvar(Codigo3 c3, int action){//no sirve!!!
//		switch(action){
//		case NAME:
//			return c3.getTSVar().getTupla(IP).getNombreID();
//
//		case VALUE_I:
//			return c3.getTSVar().getTupla(IP).getValorI();
//			
//		case VALUE_F:
//			return c3.getTSVar().getTupla(IP).getValorF();
//			
//		case NUMBER_TMP:
//			return c3.getTSVar().getTupla(IP).getCantTmp();
//		}
//		
//		return null;
//	}
	/**
	 * @param c3
	 * @param index
	 * @return retorna el valor de un campo de la tss
	 */
	private String getValueOfTss(Codigo3 c3, int index){
		return c3.getTSS().getStr(index);
	}
	/**
	 * @param c3
	 * @param action
	 * @param index
	 * @param value
	 * @return almacena valor en el campo respectivo de la tsvar
	 */
	private void setValueOfTsvar(Codigo3 c3, int action, int index, Object value){
		switch(action){
		case VALUE_I:
			c3.getTSVar().setValorI(index, (int) value);
			
		case VALUE_F:
			c3.getTSVar().setValorF(index, (int) value);
		}
	}
	/**
	 * @param c3
	 * @return guarda las direcciones de todas las etiquetas en un hash map
	 */
	private void saveE(Codigo3 c3){
		while (IP < c3.getN()) {
			if(Cuadrupla.ETIQUETA == c3.getCuadrupla(IP).getTipoOp()){
				listE.put(c3.getCuadrupla(IP).getDir(1), IP);
			}
			IP += 1;
		}

		IP = 0;
	}
}
